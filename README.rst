Docker compose setup
====================

This will hold two submodules (telegram-channel-publisher and codea-publisher),
and the it's aimed to be used while in development.

In order to start playing with this repository, you have to setup the env file
and synchronize the submodules.

Update repository submodules and env file
-----------------------------------------

Update submodules
`````````````````

::

    $ git submodule update --init --remote

The previous command will clone the submodules into `channel-publisher` and `gui-channel-publisher`.

Setup .env file
```````````````

Just rename the `channel-publisher/.env.example` file to `.env` and ask someone for
the right values (Note: .env file is a hidden file and it's expected to located
in the root path of this repository).


Getting started with docker
---------------------------

Start docker services
`````````````````````

::

    $ docker-compose -f docker-compose.json up -d

Show services status
````````````````````

::

    $ docker-compose -f docker-compose.json ps

From previous output you can see the gui is running in port 3002. So let's open
our web browser http://127.0.0.1:3002

Listen to the logs
``````````````````

::

    $ docker-compose -f docker-compose.json logs -f
